/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 14:40:35 by yaitalla          #+#    #+#             */
/*   Updated: 2016/06/30 05:50:36 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void				new_token(t_oken_type type, t_oken **tok, char *t)
{
	t_oken					*new;
	t_oken					*temp;

	new = (t_oken *)malloc(sizeof(t_oken));
	new->tok = t;
	new->type = type;
	if (*tok == NULL)
	{
		*tok = new;
		new->prev = NULL;
	}
	else
	{
		temp = *tok;
		while (temp->next)
			temp = temp->next;
		temp->next = new;
		new->prev = temp;
	}
	new->next = NULL;
}

static int				no_redir(t_oken **tok, char *cmd, int i)
{
	int						ret;

	ret = 0;
	while (P && !SPCHECK && !ft_isspace(P))
		ret++;
	if (ret > 0)
		new_token(WORD, tok, ft_strsub(cmd, i, ret));
	return (ret);
}

static int				redir(t_oken **tok, char *cmd, int i)
{
	if (cmd[i + 1] == cmd[i])
	{
		if (cmd[i] == '<')
			new_token(REDIR_LL, tok, ft_strsub(cmd, i, 2));
		if (cmd[i] == '>')
			new_token(REDIR_RR, tok, ft_strsub(cmd, i, 2));
		if (cmd[i] == '|')
			new_token(OR, tok, ft_strsub(cmd, i, 2));
		if (cmd[i] == '&')
			new_token(AND, tok, ft_strsub(cmd, i, 2));
		return (2);
	}
	else
	{
		if (cmd[i] == ';')
			new_token(SEMICOL, tok, ft_strsub(cmd, i, 1));
		if (cmd[i] == '>')
			new_token(REDIR_R, tok, ft_strsub(cmd, i, 1));
		if (cmd[i] == '|')
			new_token(PIPE, tok, ft_strsub(cmd, i, 1));
		if (cmd[i] == '<')
			new_token(REDIR_L, tok, ft_strsub(cmd, i, 1));
		return (1);
	}
}

t_oken					*create_token(char *cmd)
{
	t_oken					*tok;
	int						i;

	tok = NULL;
	i = 0;
	while (cmd[i])
	{
		while (ft_isspace(cmd[i]))
			i++;
		i += S_CHECK ? redir(&tok, cmd, i) : no_redir(&tok, cmd, i);
	}
	if (parse_error(tok))
	{
		token_destroyer(&tok);
		return (NULL);
	}
	return (tok);
}
