/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabplusone.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 23:56:13 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/09 19:11:38 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "colors.h"

void		tabtabplus(char **av, char *str)
{
	char		**table;
	int			i;
	int			j;
	int			k;

	k = 0;
	j = ft_tablen(av);
	i = 0;
	table = (char **)malloc(sizeof(char *) * j + 1);
	while (i < j)
	{
		table[i] = ft_strdup(av[i]);
		i++;
	}
	table[i++] = ft_strdup(str);
	table[i] = NULL;
	av = table;
}
