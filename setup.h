/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/12 16:36:53 by yaitalla          #+#    #+#             */
/*   Updated: 2016/06/30 05:38:32 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SETUP_H
# define SETUP_H

typedef struct			s_hist
{
	int					len;
	int					ok;
	int					index;
	char				*save;
	char				**hist;
}						t_hist;

typedef struct			s_enable
{
	int					hist;
	int					insert;
	int					copy_paste;
}						t_enable;

typedef struct			s_ccp
{
	char				*str;
}						t_ccp;

typedef struct			s_setup
{
	int					test;
	int					prompt;
	int					curs;
	int					posi;
	int					c_len;
	char				*cmd;
	int					prompt_len;
	int					line;
	int					max;
	int					cursor;
	int					pos;
	int					cmdlen;
}						t_setup;

#endif
