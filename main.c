/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/12 12:35:51 by yaitalla          #+#    #+#             */
/*   Updated: 2016/05/29 18:45:18 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int					main(void)
{
	extern char			**environ;
	t_shell				*shell;

	signaler();
	shell = init_all(environ);
	reset(0, shell);
	while (1)
		prompt(shell);
	termclose(shell);
	return (0);
}
