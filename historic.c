/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   historic.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 08:34:25 by yaitalla          #+#    #+#             */
/*   Updated: 2016/06/30 05:48:21 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			display_historic(t_shell *shell)
{
	int				i;

	i = 0;
	while (i < HST->len)
	{
		ft_putstr("    #");
		ft_putnbr(i + 1);
		ft_putstr(" : ");
		ft_putendl(HST->hist[i]);
		i++;
	}
}

static void		check_line(t_shell *shell, int len)
{
	len += SET.prompt_len;
	SET.max = 1;
	SET.line = 1;
	if (len >= COL)
	{
		while (len >= COL)
		{
			SET.max++;
			SET.line++;
			len -= COL;
		}
	}
}

static int		histup(t_shell *shell)
{
	if (HST->index > 0)
	{
		ENABLE.hist = 1;
		HST->index--;
		clear_line(shell);
		SET.cursor = ft_strlen(CMD);
		SET.cmdlen = SET.cursor;
		prompter(shell);
		ft_putstr(CMD);
		check_line(shell, SET.cmdlen);
	}
	return (SET.cmdlen + SET.prompt_len + 1);
}

static int		histdown(t_shell *shell)
{
	if (HST->index < HST->len - 1)
	{
		clear_line(shell);
		HST->index++;
		SET.cursor = ft_strlen(CMD);
		SET.cmdlen = SET.cursor;
		prompter(shell);
		ft_putstr(CMD);
		check_line(shell, SET.cmdlen);
		return (SET.prompt_len + SET.cmdlen + 1);
	}
	else if (HST->index == (HST->len - 1))
	{
		clear_line(shell);
		HST->index++;
		SET.cursor = ft_strlen(HST->save);
		SET.cmdlen = SET.cursor;
		prompter(shell);
		ft_putstr(HST->save);
		ENABLE.hist = 0;
		check_line(shell, SET.cmdlen);
		return (SET.prompt_len + SET.cmdlen + 1);
	}
	return (SET.pos);
}

void			historic(char buf[3], t_shell *shell)
{
	if (K_UP)
		SET.pos = histup(shell);
	if (K_DOWN)
		SET.pos = histdown(shell);
	while (SET.pos > COL)
		SET.pos -= COL;
}
