/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   built_func.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 17:38:56 by yaitalla          #+#    #+#             */
/*   Updated: 2016/06/30 10:02:53 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char					*regulator(t_shell *shell, char buf[4], char *save)
{
	if (BUF == 4)
	{
		if (SET.cmdlen == 0)
			exit(0);
		if (SET.cursor != SET.cmdlen)
		{
			go_right(shell);
			return (reg_prompt(shell, backspacer(shell, save), 1));
		}
	}
	if (SET.cursor)
		return (reg_prompt(shell, backspacer(shell, save), ENABLE.insert));
	else
		return (reg_prompt(shell, SET.cmd, ENABLE.insert));
}

t_shell					*reset(int i, t_shell *shell)
{
	static t_shell			*temp = NULL;

	if (!i)
		temp = shell;
	return (temp);
}

int						one_cmd(t_ree *tree, t_shell *shell, int c)
{
	int						fdout;

	fdout = check_out(tree->cmd->fdout);
	if (is_built(tree->cmd->arg[0], shell))
		return (built(tree->cmd->arg, shell, fdout, c));
	else
		return (path_cmd(tree, shell, c, fdout));
	return (127);
}
