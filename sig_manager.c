/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sig_manager.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 17:30:52 by yaitalla          #+#    #+#             */
/*   Updated: 2016/06/30 06:20:26 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
/*
**	static void		canceller(void)
**		t_shell		*shell;
**
**		shell = NULL;
**		shell = reset(1, shell);
**		termclose(shell);
**		exit(0);
*/

static void		continuer(void)
{
	t_shell		*shell;

	shell = NULL;
	shell = reset(1, shell);
	shell->trial.term.c_lflag &= ~(ICANON | ECHO);
	shell->trial.term.c_cc[VMIN] = 1;
	shell->trial.term.c_cc[VTIME] = 0;
	tcsetattr(0, 0, &(shell->trial.term));
	signal(SIGTSTP, sig_catcher);
}

static void		stopper(void)
{
	t_shell		*shell;
	char		cp[2];

	shell = NULL;
	shell = reset(1, shell);
	cp[0] = shell->trial.term.c_cc[VSUSP];
	cp[1] = 0;
	shell->trial.term.c_lflag |= (ICANON | ECHO);
	signal(SIGTSTP, SIG_DFL);
	tcsetattr(0, 0, &(shell->trial.term));
	tputs(tgetstr("te", NULL), 1, tputchar);
	tputs(tgetstr("ve", NULL), 1, tputchar);
	ioctl(0, TIOCSTI, cp);
}

void			sig_catcher(int i)
{
	if (i == SIGCONT)
		continuer();
	else if (i == SIGTSTP)
		stopper();
}

/*
**	else if (i == SIGWINCH)
**		resizer();
**	else
**		canceller();
*/

void			signaler(void)
{
	int			i;

	i = 1;
	while (i < 32)
	{
		signal(i, sig_catcher);
		i++;
	}
}
